require 'test/unit'
require 'dtd_to_classes'
require 'construct'
class ReadNubTests < Test::Unit::TestCase
  include Construct::Helpers
  fixtures :data
  
  def load_parser(_name)
    cabecera = '<?xml version="1.0" encoding="ISO-8859-1"?>
      <!DOCTYPE TVListing SYSTEM "TVListing.dtd">
      <TVListing creationDate="20111115123441">
	    <ProviderInfo>
		  <ProviderId>MTVN</ProviderId>
		  <ProviderName>MTV Network</ProviderName>
	    </ProviderInfo>
	    <ScheduleData>
		  <ChannelPeriod beginTime="20111115080000" endTime="20111116064300">
			<ChannelId>PARA</ChannelId>'
    pie = '</ChannelPeriod></ScheduleData></TVListing>'
    xml = cabecera + data(_name)["xml"] + pie
    
    within_construct do |construct|
      construct.file "#{_name}.nub", data(_name)["nub"]
      construct.file "#{_name}.xml", xml
      @parser = Parser.new(_name)
      @event = @parser.events[0]
    end  
  end

  def test_pelicula
    load_parser('pelicula_mis_padres_son_marcianos')
    assert_nil @event
  end

  def test_serie_como_pelicula
    load_parser('serie_como_pelicula_dora_la_exploradora')
    assert_nil @event
  end

  def test_serie_con_episodio_sin_cycle
    load_parser('serie_con_episodio_sin_cycle_los_padrinos_magicos')
    assert_equal @event.code, 20111003094024
    assert_equal @event.cycle, 0
    assert_equal @event.episode_number, 19
    assert !@event.valid? 
  end    

  def test_serie_ok_los_pinguinos_de_madagascar_c1_ep22 
     load_parser('serie_ok_los_pinguinos_de_madagascar_c1_ep22')
     assert_equal @event.code, 20111003134612
     assert_equal @event.cycle, 1
     assert_equal @event.episode_number, 22
     assert @event.valid?
  end

  def test_serie_ok_impares_c3_ep80
    load_parser('serie_ok_impares_c3_ep80')
    assert_equal @event.code, 20111115101700
    assert_equal @event.cycle, 3
    assert_equal @event.episode_number, 80
    assert @event.serie?
    assert @event.valid?
  end

  def test_serie_ok_avecina_c1_ep10
    load_parser('serie_ok_avecina_c1_ep10')
    assert_equal @event.code, 20111116003000
    assert_equal @event.cycle, 1
    assert_equal @event.episode_number, 10
    assert @event.valid?
  end
    
  def test_serie_no_ok_aida_c5_ep141
    # En la linea 2 hay un /, donde deberia haber ,
    load_parser('serie_no_ok_aida_c5_ep141')
    assert_equal @event.code, 20111115185400
    assert_equal @event.cycle, 5
    assert_equal @event.episode_number, 0 # Porque hay un error en la linea 2
    assert !@event.valid?
  end
   
end
