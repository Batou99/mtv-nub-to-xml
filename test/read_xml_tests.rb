require 'test/unit'
require 'dtd_to_classes'
require 'construct'
class ReadXMLTests < Test::Unit::TestCase
  include Construct::Helpers
  fixtures :data
  
  def load_parser(_name)
    cabecera = '<?xml version="1.0" encoding="ISO-8859-1"?>
      <!DOCTYPE TVListing SYSTEM "TVListing.dtd">
      <TVListing creationDate="20111115123441">
	    <ProviderInfo>
		  <ProviderId>MTVN</ProviderId>
		  <ProviderName>MTV Network</ProviderName>
	    </ProviderInfo>
	    <ScheduleData>
		  <ChannelPeriod beginTime="20111115080000" endTime="20111116064300">
			<ChannelId>PARA</ChannelId>'
    pie = '</ChannelPeriod></ScheduleData></TVListing>'
    xml = cabecera + data(_name)["xml"] + pie
    
    within_construct do |construct|
      construct.file "#{_name}.nub", data(_name)["nub"]
      construct.file "#{_name}.xml", xml
      @parser = Parser.new(_name)
      @event = @parser.events[0]
      @tv_listing = @parser.tv_listing
      @extended_infos = @parser.tv_listing.schedule_data.channel_period.events[0].epg_production.epg_text.extended_infos
    end  
  end

  def find(_array,_value,_key='EpisodeNumber')

    encontrados=0
    _array.each { |e|  
      if e.name==_key
        assert_equal e.none,_value.to_s
        encontrados+=1
        break
      end 
    }
    encontrados
  end

  def test_pelicula
    load_parser('pelicula_mis_padres_son_marcianos')
    assert_equal @extended_infos.size, 0 
  end

  def test_serie_como_pelicula
    load_parser('serie_como_pelicula_dora_la_exploradora')
    assert_equal @extended_infos.size, 0 
  end

  def test_serie_con_episodio_sin_cycle
    load_parser('serie_con_episodio_sin_cycle_los_padrinos_magicos')
    assert @extended_infos
    assert_equal @extended_infos.size, 5
    find(@extended_infos,19)
    assert_equal find(@extended_infos,0,'Cycle'), 0
  end

  def test_serie_ok_los_pinguinos_de_madagascar
    load_parser('serie_ok_los_pinguinos_de_madagascar_c1_ep22')
    assert_equal @extended_infos.size, 5
    find(@extended_infos,22)
    assert_equal find(@extended_infos,0,'Cycle'), 0
  end
  
  def test_serie_ok_impares
    load_parser('serie_ok_impares_c3_ep80')
    assert_equal @extended_infos.size, 3
    assert_equal find(@extended_infos,22), 0
    assert_equal find(@extended_infos,0,'Cycle'), 0
  end

  def test_serie_ok_avecina
    load_parser('serie_ok_avecina_c1_ep10')
    assert_equal @extended_infos.size, 4
    assert_equal find(@extended_infos,10), 1
    assert_equal find(@extended_infos,0,'Cycle'), 0
  end

  def test_serie_no_ok_aida
    load_parser('serie_no_ok_aida_c5_ep141')
    assert_equal @extended_infos.size, 4
    assert_equal find(@extended_infos,141), 1
    assert_equal find(@extended_infos,0,'Cycle'), 0
  end
end
