require'rubygems'
require 'happymapper'

module HappyMapper
	def self.included(base)
		base.instance_variable_set("@attributes", {})
 		base.instance_variable_set("@elements", {})
 		base.instance_variable_set("@registered_namespaces", {})
 		base.extend ClassMethods
		base.class_eval do
			alias :old_to_xml :to_xml
				def to_xml(parent_node = nil, default_namespace = nil)
					data = old_to_xml(parent_node,default_namespace)			
					data=data.to_s.gsub(/<[\/]*__NONE__>/,"")   
					data.gsub(/<[A-Za-z0-9]+\/>/,"")
				end
			end
		end
end

class ProviderInfo
	include HappyMapper
	tag 'ProviderInfo'
	element :provider_id, String, :tag => "ProviderId"
	element :provider_name, String, :tag => "ProviderName"
end



class AudioInfo
	include HappyMapper
	tag 'AudioInfo'
	element :stereo_flag, Boolean, :tag => "StereoFlag"
	element :dolby_digital_flag, Boolean, :tag => 'DolbyDigitalFlag'
	element :surround_sound_flag, Boolean, :tag => 'SurroundSoundFlag'
end

class User
	include HappyMapper
	tag 'User'
	attribute :nibble1, String
	attribute :nibble2, String
end

class DvbContent
	include HappyMapper
	tag 'DvbContent'
	element :user, User, :tag => 'User', :single => true
end

class ExtendedInfo 
	include HappyMapper
	tag 'ExtendedInfo'
	attribute :name, String
	element :none, String, :tag => '__NONE__'

	def initialize(_name=nil,_none=nil)
		self.name=_name
		self.none=_none
	end
end

class EpgText
	include HappyMapper
	tag 'EpgText'
	attribute :language, String 
	element :name, String, :tag => 'Name'
	element :short_description, String, :tag => "ShortDescription"
	has_many :extended_infos, ExtendedInfo#, :attributes => {:name => String}
end

class EpgProduction
	include HappyMapper
	tag 'EpgProduction'
	element :epg_text,EpgText, :single => true
	element :parental_rating, Integer, :tag => 'ParentalRating'
	element :audio_info, AudioInfo, :single => true
	element :vo_vos, Integer, :tag => 'VoVos'
	element :live_flag, Boolean, :tag => 'LiveFlag'
	element :showing, Integer, :tag => 'Showing'
	element :dvb_content, DvbContent, :single => true
	#element :extended_info, String, :tag => 'ExtendedInfo',:single => false, :attributes => {:name => String}

end

class Event
	include HappyMapper
	tag 'Event'
	attribute :begin_time, String, :tag => 'beginTime'
	attribute :duration, String
	element :epg_production, EpgProduction, :single => true
end

class ChannelPeriod
	include HappyMapper
	tag 'ChannelPeriod'
	attribute :begin_time, String, :tag => 'beginTime'
	attribute :end_time, String, :tag => 'endTime'
	element :channel_id, String, :tag => 'ChannelId'
	has_many :events, Event
end

class ScheduleData
	include HappyMapper
	tag 'ScheduleData'
	element :channel_period, ChannelPeriod, :single => true
end

class TVListing
	include HappyMapper
	tag 'TVListing'
	element :provider_info, ProviderInfo
	element :schedule_data, ScheduleData, :single => true
end

class NubEvent

	def initialize
		@_episode_number=0
		@_cycle=0
		@_code=0
    @_episode_name=""
		@_type="Pelicula"
	end

	def dos_1=(_line) 
		_line.gsub!(/\/,/,"")
		@_cycle = _line.split(',')[15].to_i 	
	end

	def dos_2=(_line)
		_line.gsub!(/\/,/,"")
		@_episode_number = _line.split(',')[14].to_i
		@_type="Serie"
    @_episode_name = _line.split(',')[2].gsub(/;/,',')
	end

	def code=(_line)
		_line.gsub!(/\/,/,"")
	  @_code=_line.split(',')[3].to_i
	end	

	def valid?
		@_episode_number!=0 && @_cycle!=0 && @_code!=0
	end

	def type?
		@_type
	end

  def serie?
  	@_type == "Serie" ? true : false
  end

	def code
		@_code
	end

	def episode_number
		@_episode_number
	end

  def episode_name
    @_episode_name
  end

	def cycle
		@_cycle
	end
end

class Parser

  attr :events
  attr :tv_listing
	def initialize(_file)
		nub = "#{_file}.nub"
		xml = "#{_file}.xml"
		@events = read_nub(nub)
		@tv_listing = read_xml(xml)
	end

  def get_episode_name(_event)
    _event.epg_production.epg_text.extended_infos.each { |ex|
      return ex.none if ex.name="EpisodeName"
    }
  end

  def get_event_by_name(_name)
    @events.each { |event|  
			return event if event.episode_name==_name
    }
    NubEvent.new
  end

	def get_event_by_code(_code)
		@events.each { |event|  
			return event if event.code==_code
		}
    @events.each { |event|  
			return event if event.code==_code-10000
		}
    @events.each { |event|  
			return event if event.code==_code-20000
		}
    nil		
  end

	def parse
		@tv_listing.schedule_data.channel_period.events.each { |event|  
			begin_time = event.begin_time.to_i+20000
			nub_event = get_event_by_code(begin_time)
      if !nub_event
        name = get_episode_name(event)
        nub_event = get_event_by_name(name)
      end
      #puts @events[0].inspect
      #puts begin_time
      #puts nub_event.inspect  
			if nub_event.serie?
				ep = ExtendedInfo.new("Cycle",nub_event.cycle)
				event.epg_production.epg_text.extended_infos << ep
        episode_number_instances=0
          event.epg_production.epg_text.extended_infos.each { |e|
            episode_number_instances+=1 if e.name=='EpisodeNumber'
          }
				  event.epg_production.epg_text.extended_infos << ExtendedInfo.new("EpisodeNumber",nub_event.episode_number) if episode_number_instances==0
			end
		}
	end

	def to_xml
		parse
		@tv_listing.to_xml
	end


  def read_xml(_filename)
		cadena=""
    file = File.new(_filename, "r")
    while (line = file.gets)
  	  line.gsub!(/>([^<]+)<\/ExtendedInfo>/,'><__NONE__>\1</__NONE__></ExtendedInfo>')
			line.gsub!(/&/,'&amp;')
  	  cadena+=line
    end
    file.close
    TVListing.parse(cadena)
  end
  
  	
  
  
  def read_nub(_filename)
    events=[]
    stage=0
    counter = 1
    file = File.new(_filename, "r")
    nub_event = NubEvent.new
    while (line = file.gets)
			line.gsub!(/\/,/,";")
      case line.split(',')[0].to_i
    		when 2 then
    			if stage==0
    				nub_event = NubEvent.new
    				nub_event.dos_1 = line
    				stage=stage+1
    			else
    				nub_event.dos_2 = line
    			end
    		when 8 then
    			stage=0
    			nub_event.code = line
    			events << nub_event if nub_event.valid? || nub_event.serie?
    	end
    end
    events	
  end

end


#
#a = EpgProduction.new
#ex = ExtendedInfo.new
#ex.name = "ejemploex"
#ex.value = "a ver"
#ex2 = ExtendedInfo.new
#ex2.name = "ex2"
#ex2.value = "trulloak"
#
#a.extended_info = [ex,ex2] 
##a.extended_info.name = "nombre"
#puts a.to_xml
##data.gsub!(/<[\/]*__NONE__>/,"")

#counter = 1
#cadena=""
#file = File.new("fixtures/PARAMOUNT (24OCTUBRE).xml", "r")
#while (line = file.gets)
	#line.gsub!(/>([^<]+)<\/ExtendedInfo>/,'><__NONE__>\1</__NONE__></ExtendedInfo>')
	#cadena+=line
#end
#file.close
##puts cadena
##cadena.gsub!(/<ExtendedInfo name=[A-Za-z]+>/)
#tv_listing = TVListing.parse(cadena)
##puts tv_listing.schedule_data.inspect
#ex = ExtendedInfo.new
#ex.name = "nombre"
#ex.none = "hez"
#tv_listing.schedule_data.channel_period.events[0].epg_production.epg_text.extended_infos << ex
##puts tv_listing.schedule_data[0].class
#puts tv_listing.to_xml
