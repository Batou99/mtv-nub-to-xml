require 'rubygems'
require 'happymapper'
require 'roo'

module HappyMapper
	def self.included(base)
		base.instance_variable_set("@attributes", {})
 		base.instance_variable_set("@elements", {})
 		base.instance_variable_set("@registered_namespaces", {})
 		base.extend ClassMethods
		base.class_eval do
			alias :old_to_xml :to_xml
				def to_xml(parent_node = nil, default_namespace = nil)
					data = old_to_xml(parent_node,default_namespace)			
					data = data.to_s.gsub(/<[\/]*__NONE__>/,"")  # Eliminamos los tags __NONE__
					data.to_s.gsub(/c[0-9]{1,2}=""/,"") # Eliminamos los atributos vacios
				end
			end
		end
end

class Field
	include HappyMapper
	tag 'Field'
	attribute :alias,String
	attribute :name,String
	attribute :type,String
	def initialize(_alias,_name,_type)
		self.alias = _alias
		self.name = _name
		self.type = _type
	end
end

class RowDefinition
	include HappyMapper
	tag 'RowDefinition'
	attribute :count,String
	has_many :fields, Field
end

class Row
	include HappyMapper
	tag 'row'
	attribute :c1, String
	attribute :c2, String
  attribute :c3, String
	attribute :c4, String
	attribute :c5, String
	attribute :c6, String
  attribute :c7, String
	attribute :c8, String
	attribute :c9, String
	attribute :c10, String
  attribute :c11, String
	attribute :c12, String
	attribute :c13, String
	attribute :c14, String
  attribute :c15, String
	attribute :c16, String
end

class Datos
	include HappyMapper
	tag 'Data'
	attribute :count,String
	has_many :rows,Row
end

class SharpGridExport
	include HappyMapper
	tag 'SharpGridExport'
	element :row_definition,RowDefinition
	element :data,Data
end

class Contador

	def initialize(_xls)
		@contador_normal=0
		@contador_publi=0
		@en_publi=false
		@xls=_xls
	end
	
	def caso_normal
		@contador_normal+=1
	end

	def de_normal_a_publi(_line)
		@contador_publi=(@xls.cell(_line,1)[0..1] + @xls.cell(_line,1)[3..4] + '0001')
	end

	def caso_publi
		@contador_publi=@contador_publi.to_i+1
		ret = @contador_publi.to_s
		ret = '0'+ret if ret.size<8
		return ret
	end

	def de_publi_a_normal
		@contador_publi=0
		@contador_normal+=2
	end

	def next(_line)
		if !@en_publi
			if @xls.cell(_line,4)==""
				@en_publi = true
				return de_normal_a_publi(_line)
			else
				return caso_normal
			end
		else
			if @xls.cell(_line,4)==""
				ret = caso_publi.to_s
				ret = '0'+ret if ret.size<8
				return ret			
			else
				@en_publi = false
				return de_publi_a_normal
			end
		end
	end

end

field_key = Field.new('c1','KEY','8')
time_key = Field.new('c2','Time','8')
dur_key = Field.new('c3','Duration','8')
media_key = Field.new('c4','Media no','8')
gpi1_key = Field.new('c5','GPI 1 desc','8')
title_key = Field.new('c6','Title','8')
episode_key = Field.new('c7','Episode number','8')
production_key = Field.new('c8','Production no','8')
part_key = Field.new('c9','Part','8')
som_key = Field.new('c10','SOM','8')
event_key = Field.new('c11','Event','8')
gpi3_key = Field.new('c12','GPI 3 desc','8')
gpi29_key = Field.new('c13','GPI 29','8')
gpi2_key = Field.new('c14','GPI 2','8')
gpi11_key = Field.new('c15','GPI 11 desc','8')
audio_key = Field.new('c16','Audio Tracks','8')

rowdef = RowDefinition.new
rowdef.count='16'
#rowdef.fields = [field_key,time_key]
rowdef.fields = [field_key,time_key,dur_key,media_key,gpi1_key,title_key,episode_key,production_key,part_key,som_key,event_key,gpi3_key,gpi29_key,gpi2_key,gpi11_key,audio_key]

if ARGV.size!=1
	puts "uso: xls_to_xml nombre_del_fichero.xls"
else
  data = Datos.new
  xls = Excel.new(ARGV[0])
  xls.default_sheet = xls.sheets.first
  
  contador = Contador.new(xls)
  data.count = xls.last_row.to_s 
  data.rows = []

  1.upto(xls.last_row) do |line|
  	row = Row.new
  	2.upto(16) do |col|
  		row.send("c#{col}=",xls.cell(line,col-1).to_s)
  	end
	  c1 = contador.next(line).to_s
		if c1.size<4
			c1 = "%04d" % c1
		end
  	row.c1 = c1.to_s   	
		data.rows << row
  end
  
	sharp = SharpGridExport.new
	sharp.row_definition = rowdef
	sharp.data = data

	f = File.new("#{ARGV[0].split('.')[0]}.xml","w")
	f.write(sharp.to_xml)
	f.close
end
